<?php

/**
 * Menu callback to publish a feed for all nodes and comments by a specified author.
 *
 * @param $author_uid
 *    uid of the author whose content will appear in the feed
 */
function ixda_author_rss_feed_author($author_uid = NULL) {
  if (isset($author_uid) && is_numeric($author_uid)) {
    // Get user name, confirming user exists.
    $author_name = db_result(db_query("SELECT name FROM {users} WHERE status = 1 AND uid = %d", $author_uid));
    if ($author_name) {
      // Obtain items for author.
      $items = ixda_author_rss_format_items($author_uid);
      // Fill channel information based on author.
      $channel = array(
        'title' => t(
          '@site - @feed_title',
          array(
            '@site' => check_plain(variable_get('site_name', 'Drupal')),
            '@feed_title' => ixda_author_rss_feed_title($author_name),
          )
        ),
        'description' => ixda_author_rss_feed_title($author_name),
        'link' => ixda_author_rss_feed_url($author_uid, $absolute=TRUE),
      );
      ixda_author_rss_format_feed($items, $channel);
      return;
    }
  }
  drupal_not_found();
}

/*
 * Implementation of hook_user.
 *
 * Add RSS feed to user profile page.
 */
function ixda_author_rss_user($type, $edit, $user, $category) {
  switch ($type) {
    case 'view':
      drupal_add_feed(ixda_author_rss_feed_url($user->uid), ixda_author_rss_feed_title($user->name));
  }
}

/**
 * Format and print a complete node plus comment feed.
 * (based on commentrss_format_feed)
 *
 * @TODO: Resolve http://validator.w3.org/feed/docs/warning/MissingAtomSelfLink.html
 *   Note also being tackled in D7 http://drupal.org/node/365498
 * @param $items
 *   XML fragment for the items. @see commentrss_format_items()
 * @param $channel
 *   Associative array of channel information. Name of tags to include
 *   with value of tag values to add.
 * @param $namespaces
 *   Associative array of namespace arguments to add. Keyed by namespace
 *   argument name to add, for example 'xmlns:dc', with the value of the
 *   namespace URI such as http://purl.org/dc/elements/1.1/ in this case.
 */
function ixda_author_rss_format_feed($items, $channel = array(), $namespaces = array()) {
  global $base_url, $language;

  $channel_defaults = array(
    'version'     => '2.0',
    'link'        => $base_url,
    'language'    => $language->language,
  );
  $channel = array_merge($channel_defaults, $channel);
  $namespaces = array_merge(array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/'), $namespaces);

  // Add the atom namespace recommended by W3C validator.
  $namespaces = array_merge(array('xmlns:atom' => 'http://www.w3.org/2005/Atom'), $namespaces);
  
  $output = '<?xml version="1.0" encoding="utf-8"?>'."\n";
  $output .= '<rss version="'. $channel['version'] .'" xml:base="'. $base_url .'"'. drupal_attributes($namespaces) .">\n";

  $output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language']);
  $output .= "</rss>\n";

  drupal_set_header('Content-Type: application/rss+xml; charset=utf-8');
  print $output;
}

/**
 * Provide the URL to the user feed. (Also for use by other modules.)
 *
 * @param $author_uid
 *   uid of author whose nodes & comments are to be fed.
 * @param $absolute
 *   Whether to format as an absolute or relative URL.
 * @return
 *   Absolute or relative URL of author feed.
 */
function ixda_author_rss_feed_url($author_uid, $absolute=TRUE) {
  $url = url('user/'. strval($author_uid) .'/contentfeed', array('absolute' => $absolute));
  return ($url);
}

/**
 * Provide the title of the user feed for use in theme_feed_icon(), for example. (Also for use by other modules.)
 *
 * @param $author_name
 *   name of author whose nodes & comments are to be fed.
 * @return
 *   Title string.
 */
function ixda_author_rss_feed_title($author_name) {
  return (t('Posts and comments by @author', array('@author' => check_plain($author_name))));
}

/**
 * Format RSS for nodes and comments by an author.
 * (partly cribbed from commentrss_format_items)
 *
 * @param $author_uid
 *   uid of author whose nodes & comments are to be fed.
 * @return
 *   Formatted XML of RSS items generated.
 */
function ixda_author_rss_format_items($author_uid) {
  global $base_url;

  $extra = array(
    'pubDate' => array('key' => 'pubDate'),
    'dc:creator' => array('key' => 'dc:creator'),
    'guid' => array('key' => 'guid', 'attributes' => array('isPermaLink' => 'false')),
  );

  // Return only nodes and comments to nodes of these types.
  // @TODO: Refactor as a site variable.
  $content_types_included = '"discussion", "event", "local_discussion", "blogentry", "resource", "job"';

  // Get a specific author's nodes and comments by the union of two orthogonal queries on comments and nodes.
  // As we process the bunch, nodes (vs. comments) can be identified by cid == 0.
  $nodes_query = '
    SELECT 
      n.nid, 
      0 as cid, 
      nr.title AS subject, 
      nr.body, 
      nr.timestamp, 
      nr.format,
      n.uid, 
      u.name AS name, 
      u.name AS username, 
      nr.title,
      n.type
    FROM {node} n 
      INNER JOIN {users} u ON n.uid = u.uid 
      INNER JOIN {node_revisions} nr ON n.nid = nr.nid AND n.vid = nr.vid
    WHERE n.uid = %d AND n.status = 1 AND
      n.type IN ('. $content_types_included .')';

  $comments_query = '
    SELECT 
      n2.nid, 
      c.cid, 
      c.subject, 
      c.comment AS body, 
      c.timestamp, 
      c.format,
      c.uid, 
      c.name, 
      u2.name AS username, 
      n2.title,
      n2.type
    FROM {node} n2 
      INNER JOIN {comments} c ON c.nid = n2.nid 
      INNER JOIN {users} u2 ON c.uid = u2.uid 
    WHERE c.uid = %d AND n2.status = 1 AND c.status = %d AND
      n2.type IN ('. $content_types_included .')';

  // Note alias on first query, but not second, avoids mysql warning.
  $sql = 'SELECT * FROM ('. $nodes_query .') AS aa UNION ('. $comments_query .') ORDER BY timestamp DESC';

  // Not using query rewriting because it seems to bork the result.
  $nodes_and_comments = db_query_range($sql, $author_uid, $author_uid, COMMENT_PUBLISHED, 0, variable_get('feed_default_items', 10));
  
  $items = '';
  $item_length = variable_get('feed_item_length', 'teaser');

  // Handle each piece of author content, be it node or comment.
  while ($author_content = db_fetch_object($nodes_and_comments)) {

    
    // Handle comment content (cid != 0).
    if ($author_content->cid) {
      $items .= ixda_author_rss_render_comment($author_content, $item_length);
    }
    // Handle node content.
    else {
      $items .= ixda_author_rss_render_node($author_content->nid, $item_length);
    }
  }
  return $items;
}

/**
 * Build up comment content for use in a feed.
 * Cribbed from commentrss_format_items().
 *
 * @param $comment
 *   Object with elements title (of node), subject (of comment), nid, cid, timestamp, name, body.
 * @param $item_length
 *   Content to include in feed: 'teaser', 'title' or 'fulltext'
 *   Note: There is no sense of "teaser" for comments, so 'teaser' treated as 'fulltext'.
 * @return
 *   Formatted XML for one feed item.
 */
function ixda_author_rss_render_comment($comment, $item_length = 'fulltext') {
  global $base_url;

  // Build up item content starting with node title.
  $content = '<p>'. t('In reply to <a href="@url">@title</a>', array('@url' => url('node/'. $comment->nid, array('absolute' => TRUE)), '@title' => $comment->title)) .":</p>\n\n";

  // Format RSS item with input format used on comment. The format
  // was validated previously and the visitor might not have access to that,
  // so skip the format validation in check_markup() itself.
  if ($item_length != 'title') {
    $content .= check_markup($comment->body, $comment->format, FALSE);
  }

  $link = url('node/'. $comment->nid, array('fragment' => 'comment-'. $comment->cid, 'absolute' => TRUE));

  // Username takes precedence. The comment's name attribute should only
  // be filled in anyway if it was an anonymous comment with submitter
  // details enabled.
  if (!empty($comment->username)) {
    $comment_name = $comment->username;
  }
  else {
    $comment_name = $comment->name;
  }
  // Fall back on anonymous name if no other name was specified.
  if (empty($comment_name)) {
    $comment_name = variable_get('anonymous', 'Anonymous');
  }

  $extra = array(
    array('key' => 'pubDate', 'value' => gmdate('r', $comment->timestamp)), 
    array('key' => 'dc:creator', 'value' => $comment_name), 
    array('key' => 'guid', 'value' => 'comment ' . $comment->cid . ' at ' . $base_url, 'attributes' => array('isPermaLink' => 'false')),
  );

  return (format_rss_item($comment->subject, $link, $content, $extra));
}

/**
 * Build up node content for use in a feed.
 * Cribbed from node_feed().
 *
 * @param $nid
 *   id of a node to be treated as a feed item.
 * @param $item_length
 *   Content to include in feed: 'teaser', 'title' or 'fulltext'.
 * @return
 *   Formatted XML for one feed item.
 */
function ixda_author_rss_render_node($nid, $item_length = 'teaser') {
  global $base_url;

  // Load the specified node.
  $item = node_load($nid);
  $item->build_mode = NODE_BUILD_RSS;
  $item->link = url("node/$nid", array('absolute' => TRUE));

  if ($item_length != 'title') {
    $teaser = ($item_length == 'teaser') ? TRUE : FALSE;

    // Filter and prepare node teaser
    if (node_hook($item, 'view')) {
      $item = node_invoke($item, 'view', $teaser, FALSE);
    }
    else {
      $item = node_prepare($item, $teaser);
    }

    // Allow modules to change $node->content before the node is rendered.
    node_invoke_nodeapi($item, 'view', $teaser, FALSE);

    // Set the proper node property, then unset unused $node property so that a
    // bad theme can not open a security hole.
    $content = drupal_render($item->content);
    if ($teaser) {
      $item->teaser = $content;
      unset($item->body);
    }
    else {
      $item->body = $content;
      unset($item->teaser);
    }
  
    // Allow modules to modify the fully-built node.
    node_invoke_nodeapi($item, 'alter', $teaser, FALSE);
  }

  // Allow modules to add additional item fields and/or modify $item
  $extra = node_invoke_nodeapi($item, 'rss item');
  $extra = array_merge($extra, 
    array(
      array('key' => 'pubDate', 'value' => gmdate('r', $item->created)), 
      array('key' => 'dc:creator', 'value' => $item->name), 
      array('key' => 'guid', 'value' => 'post '. $item->nid .' at '. $base_url, 'attributes' =>
        array('isPermaLink' => 'false'),
      ),
    )
  );

  $namespaces = array();
  foreach ($extra as $element) {
    if (isset($element['namespace'])) {
      $namespaces = array_merge($namespaces, $element['namespace']);
    }
  }

  // Prepare the item description
  switch ($item_length) {
    case 'fulltext':
      $item_text = $item->body;
      break;
    case 'teaser':
      $item_text = $item->teaser;
      if (!empty($item->readmore)) {
        $item_text .= '<p>'. l(t('read more'), 'node/'. $item->nid, array('absolute' => TRUE, 'attributes' => array('target' => '_blank'))) .'</p>';
      }
      break;
    case 'title':
      $item_text = '';
      break;
  }
  return (format_rss_item($item->title, $item->link, $item_text, $extra));
}

/**
 * Implementation of hook_menu().
 */
function ixda_author_rss_menu() {
  $items = array();

  // Create URL for author RSS feed.
  $items['user/%/contentfeed'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'ixda_author_rss_feed_author',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    );
  return $items;
}

